
import {Switch , Route, Redirect} from 'react-router-dom'
import Login from '../login/login'
import Register from '../login/register'
import Home from '../home/home'
export default function RouterMain(){

  let userLoggedIn = localStorage.getItem('user')

return( 

    <Switch >
     <Route exact path='/' redirectTo='/login' >
        {userLoggedIn ? <Redirect to="/home" /> : <Login />}
     </Route>

     <Route path = '/login' component={Login} />
     <Route path='/register' component={Register} />
     <Route path='/home' component={Home} />

    </Switch>)
}
 

   
